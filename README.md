# Markdown examples

Some examples of using Markdown. Especially the new Bitbucket Labs feature that allows for some HTML tags to be embedded in Markdown.

# HTML entities

This is finicky, but if you wrap HTML entities in p tags then they will render.

<p>foo &copy; bar &mu;s quux M&Omega; blammo</p>
